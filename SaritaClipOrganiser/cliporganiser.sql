-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 26, 2017 at 05:21 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cliporganiser`
--

-- --------------------------------------------------------

--
-- Table structure for table `clips`
--

CREATE TABLE IF NOT EXISTS `clips` (
`id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `duration` double DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `isAdvertising` tinyint(4) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `max_play` int(11) DEFAULT NULL,
  `min_play` int(11) DEFAULT NULL,
  `genre` varchar(45) DEFAULT NULL,
  `show_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clips`
--

INSERT INTO `clips` (`id`, `name`, `duration`, `type`, `isAdvertising`, `frequency`, `max_play`, `min_play`, `genre`, `show_id`) VALUES
(4, 'Clip1', 1, 'Audio', 1, 2, 3, 4, 'ggggg', NULL),
(5, 'Clip2', 10, 'Audio', 1, 10, 10, 5, 'Genre1', NULL),
(6, 'Clip3', 11, 'Video', 0, 11, 11, 11, 'gggg', NULL),
(7, 'Clip4', 10, 'Audio', 0, 10, 10, 5, 'genre2', NULL),
(8, 'Clip5', 20, 'Video', 1, 10, 1, 1, 'Genre5', NULL),
(9, 'New Clip', 30, 'Video', 0, 11, 1, 1, 'anew Genre', NULL),
(10, 'clip7', 1, 'Audio', 1, 2, 1, 1, 'gxg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE IF NOT EXISTS `schedule` (
`id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `duration` double DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `date`, `duration`) VALUES
(1, '2017-09-23', 100),
(3, '2017-09-25', 100);

-- --------------------------------------------------------

--
-- Table structure for table `schedule_show`
--

CREATE TABLE IF NOT EXISTS `schedule_show` (
  `schedule_id` int(11) DEFAULT NULL,
  `show_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule_show`
--

INSERT INTO `schedule_show` (`schedule_id`, `show_id`) VALUES
(1, 1),
(1, 2),
(0, 1),
(0, 2),
(0, 3),
(3, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `shows`
--

CREATE TABLE IF NOT EXISTS `shows` (
`id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `duration` double DEFAULT NULL,
  `audio_content` double DEFAULT NULL,
  `video_content` double DEFAULT NULL,
  `advertising` double DEFAULT NULL,
  `voiceover` double DEFAULT NULL,
  `isAudio` tinyint(4) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `genre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shows`
--

INSERT INTO `shows` (`id`, `name`, `duration`, `audio_content`, `video_content`, `advertising`, `voiceover`, `isAudio`, `frequency`, `genre`) VALUES
(1, 'Show1', 10, 10, 10, 10, 10, 1, 10, 'genre1'),
(2, 'Show2', 10, 10, 10, 10, 10, 1, 10, 'genre2'),
(3, 'Show3', 10, 10, 10, 10, 10, 1, 10, 'Genre1'),
(4, 'Show4', 20, 20, 20, 20, 20, 0, 2, 'Genre4'),
(5, 'New Show', 10, 10, 10, 10, 10, 0, 10, 'New Genre'),
(6, 'show2', 2, 3, 6, 1, 9, 1, 1, '3'),
(7, 'Show5', 1, 1, 2, 3, 6, 1, 2, '6');

-- --------------------------------------------------------

--
-- Table structure for table `show_clips`
--

CREATE TABLE IF NOT EXISTS `show_clips` (
  `show_id` int(11) NOT NULL,
  `clip_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `show_clips`
--

INSERT INTO `show_clips` (`show_id`, `clip_id`) VALUES
(1, 4),
(1, 5),
(2, 5),
(2, 6),
(3, 6),
(3, 7),
(4, 7),
(4, 8),
(5, 4),
(5, 6),
(5, 9),
(6, 4),
(7, 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `username`, `password`, `role`) VALUES
(1, 'Sarita', 'Tamang', 'sarita', 'tmg', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clips`
--
ALTER TABLE `clips`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shows`
--
ALTER TABLE `shows`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clips`
--
ALTER TABLE `clips`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `shows`
--
ALTER TABLE `shows`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
