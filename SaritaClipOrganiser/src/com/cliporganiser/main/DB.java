/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cliporganiser.main;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sarita
 */

public class DB {
   private static String connection_string = "jdbc:mysql://localhost/clipOrganiser?user=root&password=";
    public static int insertUpdate(String sql) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection(connection_string);
            Statement statement = connection.createStatement();
            statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

            ResultSet rs = statement.getGeneratedKeys();
            int last_insert_id = 0;
            if (rs.next()) 
                last_insert_id = rs.getInt(1);
            rs.close();
            statement.close();
            connection.close();
            return last_insert_id;
            
        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
    }

    public static List select(String sql) {
        Connection connection;
        ResultSet resultSet;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(connection_string);
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            List<Map> results = new ArrayList<Map>();
            while (resultSet.next()) {
                int count = resultSet.getMetaData().getColumnCount();
                Map map = new HashMap();
                for (int i = 0; i < count; i++) {
                    map.put(resultSet.getMetaData().getColumnName(i + 1), resultSet.getObject(i + 1));
                }
                results.add(map);
            }
            resultSet.close();
            statement.close();
            connection.close();
            return results;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
}
