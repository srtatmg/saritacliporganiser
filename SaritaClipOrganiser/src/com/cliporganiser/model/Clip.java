package com.cliporganiser.model;

import com.cliporganiser.main.DB;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sarita
 */

public class Clip {
    private int id;
    private String name;
    private double duration;
    private String type;
    private boolean isAdvertising;
    private int frequency;
    private int max_play;
    private int min_play;
    private String genre;    

    public Clip() {
    }

    public Clip(int id) {
        List clips= DB.select("select * from clips where id="+id);
        Map clip=(Map) clips.get(0);                
        this.id = id;
        this.name = (String) clip.get("name");
        this.duration = (double) clip.get("duration");
        this.type = (String) clip.get("type");
        this.isAdvertising = (int)clip.get("isAdvertising")==1?true:false;
        this.frequency = (int) clip.get("frequency");
        this.max_play = (int) clip.get("max_play");
        this.min_play = (int) clip.get("min_play");
        this.genre = (String) clip.get("genre");
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isIsAdvertising() {
        return isAdvertising;
    }

    public void setIsAdvertising(boolean isAdvertising) {
        this.isAdvertising = isAdvertising;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getMax_play() {
        return max_play;
    }

    public void setMax_play(int max_play) {
        this.max_play = max_play;
    }

    public int getMin_play() {
        return min_play;
    }

    public void setMin_play(int min_play) {
        this.min_play = min_play;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
    public void saveClip(){
        String sql="INSERT INTO `clips`(`name`,`duration`,`type`,`isAdvertising`,`frequency`,`max_play`,`min_play`,`genre`)\n" +
                    "VALUES('"+this.name+"','"+this.duration+"','"+this.type+"',"+this.isAdvertising+",'"+this.frequency+"','"+this.max_play+"','"+this.min_play+"','"+this.genre+"');";        
        DB.insertUpdate(sql);
    }
    public void updateClip(){
        String sql="UPDATE `clips` SET \n" +
                    "`name` = '"+this.name+"',\n" +
                    "`duration` = '"+this.duration+"',\n" +
                    "`type` = '"+this.type+"',\n" +
                    "`isAdvertising` = "+this.isAdvertising+",\n" +
                    "`frequency` = '"+this.frequency+"',\n" +
                    "`max_play` = '"+this.max_play+"',\n" +
                    "`min_play` = '"+this.min_play+"',\n" +
                    "`genre` = '"+this.genre+"'\n" +
                    "WHERE `id` = '"+this.id+"';";
        DB.insertUpdate(sql);
    }
    public void deleteClip(){
        String sql="DELETE FROM `clips` WHERE id='"+this.id+"'";
        DB.insertUpdate(sql);
    }
    public List<Clip> getClips() {
        List returnClips = new ArrayList<Show>();
        List clips = DB.select("select * from clips");
        for (Object obj : clips) {

            Map clip = (Map) obj;
            Clip clipObj=new Clip();
            clipObj.id = (int) clip.get("id");
            clipObj.name = (String) clip.get("name");
            clipObj.duration = (double) clip.get("duration");
            clipObj.type = (String) clip.get("type");
            clipObj.isAdvertising = (int)clip.get("isAdvertising")==1?true:false;
            clipObj.frequency = (int) clip.get("frequency");
            clipObj.max_play = (int) clip.get("max_play");
            clipObj.min_play = (int) clip.get("min_play");
            clipObj.genre = (String) clip.get("genre");
            
        returnClips.add(clipObj);

        }
        return returnClips;
    }
    
    
}
