
package com.cliporganiser.model;

import com.cliporganiser.main.DB;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sarita
 */

public class Schedule {
    private int id;
    private String date;
    private double duration;
    private List<Show> shows;

    public Schedule() {
    }

    public Schedule(int id) {
        this.id = id;
        List clips= DB.select("select * from schedule where id="+id);
        Map clip=(Map) clips.get(0);
        this.date = clip.get("date")+"";
        this.duration = (double) clip.get("duration");
        List<Show> shows=new ArrayList();
        List showsList=DB.select("select * from schedule_show where schedule_id="+id);
        for (Object object : showsList) {
            Map showObj=(Map) object;
            int showId=(int) showObj.get("show_id");
            Show show=new Show(showId);
            shows.add(show);
        }
        this.shows=shows;
        
    }
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public List<Show> getShows() {
        return shows;
    }

    public void setShows(List<Show> shows) {
        this.shows = shows;
    }
    public void save(){
        String sql="INSERT INTO `schedule`(`date`,`duration`)VALUES('"+date+"','"+duration+"');";
        int inserted_id=DB.insertUpdate(sql);
        for (Show show : shows) {
            sql="INSERT INTO `schedule_show`(`schedule_id`,`show_id`)VALUES('"+inserted_id+"','"+show.getId()+"');";
            DB.insertUpdate(sql);
        }
        
    }
    public List<Schedule> getAllSchedules(boolean date){
        List schedules=new ArrayList();
        String order=date?" order by date":"";
        List schedulesList= DB.select("select * from schedule"+order);
        for (Object object : schedulesList) {
            Map scheduleMap=(Map) object;
            Schedule schedule=new Schedule((int) scheduleMap.get("id"));            
            schedules.add(schedule);
        }
        return schedules;
    }

    public void deleteSchedule() {
        String sql = "DELETE FROM `schedule` WHERE id='" + this.id + "'";
        DB.insertUpdate(sql);
        sql="DELETE FROM `schedule_show` WHERE schedule_id='" + this.id + "'";
        DB.insertUpdate(sql);
    }
    
    
}
