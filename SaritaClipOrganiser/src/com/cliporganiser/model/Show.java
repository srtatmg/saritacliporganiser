package com.cliporganiser.model;

import com.cliporganiser.main.DB;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sarita
 */

public class Show {

    private int id;
    private String name;
    private double duration;
    private double audio_content;
    private double video_content;
    private double advertising;
    private double voiceover;
    private boolean isAudio;
    private int frequency;
    private String genre;
    private List<Clip> clips;

    public Show() {
    }

    public Show(int id) {
        List shows = DB.select("select * from shows where id=" + id);
        Map show = (Map) shows.get(0);
        this.id = id;
        this.name = (String) show.get("name");
        this.duration = (double) show.get("duration");
        this.audio_content = (double) show.get("audio_content");
        this.video_content = (double) show.get("video_content");
        this.advertising = (double) show.get("advertising");
        this.voiceover = (double) show.get("voiceover");
        this.isAudio = (int) show.get("isAudio") == 1 ? true : false;
        this.frequency = (int) show.get("frequency");
        this.genre = (String) show.get("genre");

        // for clips need to do stuff
        this.clips = new ArrayList();
        List clips_list = DB.select("select * from show_clips where show_id=" + id);
        for (Object object : clips_list) {
            Map clip = (Map) object;
            Clip clipObj = new Clip((int) clip.get("clip_id"));
            this.clips.add(clipObj);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public double getAudio_content() {
        return audio_content;
    }

    public void setAudio_content(double audio_content) {
        this.audio_content = audio_content;
    }

    public double getVideo_content() {
        return video_content;
    }

    public void setVideo_content(double video_content) {
        this.video_content = video_content;
    }

    public double getAdvertising() {
        return advertising;
    }

    public void setAdvertising(double advertising) {
        this.advertising = advertising;
    }

    public double getVoiceover() {
        return voiceover;
    }

    public void setVoiceover(double voiceover) {
        this.voiceover = voiceover;
    }

    public boolean isIsAudio() {
        return isAudio;
    }

    public void setIsAudio(boolean isAudio) {
        this.isAudio = isAudio;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public List<Clip> getClips() {
        return clips;
    }

    public void setClips(List<Clip> clips) {
        this.clips = clips;
    }

    public void saveShow() {
        String sql = "INSERT INTO `shows`(`name`,`duration`,`audio_content`,`video_content`,`advertising`,`voiceover`,`isAudio`,`frequency`,`genre`)\n"
                + "VALUES('" + this.name + "','" + this.duration + "','" + this.audio_content + "','" + this.video_content + "','" + this.advertising + "','" + this.voiceover + "'," + this.isAudio + ",'" + this.frequency + "','" + this.genre + "');";
        int insertedId = DB.insertUpdate(sql);
        for (Clip clip : this.clips) {
            sql = "INSERT INTO `show_clips`(`show_id`,`clip_id`) VALUES('" + insertedId + "','" + clip.getId() + "')";
            DB.insertUpdate(sql);
        }
    }

    public void updateShow() {

        String sql = "UPDATE `shows` SET\n"
                + "`name` = '" + this.name + "',\n"
                + "`duration` = '" + this.duration + "',\n"
                + "`audio_content` = '" + this.audio_content + "',\n"
                + "`video_content` = '" + this.video_content + "',\n"
                + "`advertising` = '" + this.advertising + "',\n"
                + "`voiceover` = '" + this.voiceover + "',\n"
                + "`isAudio` = " + this.isAudio + ",\n"
                + "`frequency` = '" + this.frequency + "',\n"
                + "`genre` = '" + this.genre + "'\n"
                + "WHERE `id` = '" + this.id + "';";
        DB.insertUpdate(sql);

        DB.insertUpdate("delete from `show_clips` where `show_id`=" + this.id);
        for (Clip clip : this.clips) {
            sql = "INSERT INTO `show_clips`(`show_id`,`clip_id`) VALUES('" + this.id + "','" + clip.getId() + "')";
            DB.insertUpdate(sql);
        }

    }

    public void deleteShow() {
        String sql = "DELETE FROM `shows` WHERE id='" + this.id + "'";
        DB.insertUpdate(sql);
        sql="DELETE FROM `show_clips` WHERE show_id='" + this.id + "'";
        DB.insertUpdate(sql);
    }

    public List<Show> getShows() {
        List returnShows = new ArrayList<Show>();
        List shows = DB.select("select * from shows");
        for (Object obj : shows) {

            Map show = (Map) obj;
            Show showObj=new Show();
            showObj.id = (int) show.get("id");
            showObj.name = (String) show.get("name");
            showObj.duration = (double) show.get("duration");
            showObj.audio_content = (double) show.get("audio_content");
            showObj.video_content = (double) show.get("video_content");
            showObj.advertising = (double) show.get("advertising");
            showObj.voiceover = (double) show.get("voiceover");
            showObj.isAudio = (int) show.get("isAudio") == 1 ? true : false;
            showObj.frequency = (int) show.get("frequency");
            showObj.genre = (String) show.get("genre");

            // for clips need to do stuff
            showObj.clips = new ArrayList();
            List clips_list = DB.select("select * from show_clips where show_id=" + showObj.id);
            for (Object object : clips_list) {
                Map clip = (Map) object;
                Clip clipObj = new Clip((int) clip.get("clip_id"));
                showObj.clips.add(clipObj);
            }
        returnShows.add(showObj);

        }
        return returnShows;
    }

}
