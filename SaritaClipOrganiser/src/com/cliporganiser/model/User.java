
package com.cliporganiser.model;

import com.cliporganiser.main.DB;
import static com.cliporganiser.main.DB.select;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sarita
 */

public class User {
    private int id;
    private String firstname;
    private String lastname;
    private String username;
    private String password;
    private String role;

    public User() {
    }

    public User(int id) {
        List users= DB.select("select * from users where id="+id);
        Map user=(Map) users.get(0);
        this.id = id;
        this.firstname = (String) user.get("firstname");
        this.lastname = (String) user.get("lastname");
        this.username = (String) user.get("username");
        this.password = (String) user.get("password");
        this.role = (String) user.get("role");
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
    public void saveUser(){
        String sql="INSERT INTO `clipOrganiser`.`users`(`firstname`,`lastname`,`username`,`password`,`role`)VALUES\n" +
                    "('"+this.firstname+"','"+this.lastname+"','"+this.username+"','"+this.password+"','"+this.role+"');";        
        DB.insertUpdate(sql);
    }
    
    
}
